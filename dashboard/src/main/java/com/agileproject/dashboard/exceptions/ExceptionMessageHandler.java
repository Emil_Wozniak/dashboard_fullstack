package com.agileproject.dashboard.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ExceptionMessageHandler extends RuntimeException {

    public ExceptionMessageHandler(String s) {
        super(s);
    }
}
